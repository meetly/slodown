module Slodown
  class Formatter
    def initialize(source)
      @current = @source = source.to_s
    end

    # Runs the entire pipeline.
    #
    def complete
      extract_metadata.markdown
    end

    # Convert the current document state from Markdown into HTML.
    #
    def markdown
      @current = Kramdown::Document.new(@current, kramdown_options).to_slodown_html
      self
    end

    def extract_metadata
      @metadata = {}

      @current = @current.each_line.drop_while do |line|
        next false if line !~ /^#\+([a-z_]+): (.*)/

        key, value = $1, $2
        @metadata[key.to_sym] = value
      end.join('')

      self
    end

    # Return a hash with the extracted metadata
    #
    def metadata
      @metadata
    end

    def to_s
      @current
    end

  private

    def kramdown_options
      { coderay_css: 'style' }
    end
  end
end
